use std::path::PathBuf;
use std::path::Path;
use std::process;



fn yes_no_prompt(question: &str) -> bool{//return true on 'y', otherwise false
	println!("{}", question);

	let mut input = String::new();
	match std::io::stdin().read_line(&mut input) {
    	Ok(_) => {
			if input.starts_with('y'){
				return true;
			}
			else if input.starts_with('n'){
				return false;
			}
    	}
    	Err(error) => {
			println!("error: {}", error);
			process::exit(1);
		},
	}
	return false;
}

trait FileDirNewer{
	fn is_newer_than(&self, other: &Path) -> bool;
}

impl FileDirNewer for Path{
	fn is_newer_than(&self, other: &Path) -> bool{
		if (self.is_dir() || self.is_file()) && (other.is_dir() || other.is_file()){//check both are either file or dir
			let time_self = self.metadata().unwrap().modified().unwrap();
			let time_other = other.metadata().unwrap().modified().unwrap();

			if time_self >= time_other{
				return true;
			}
		}
		return false;
	}
}

fn iter_dir_merge_copy(src1: &Path, src2: &Path, dest: &Path, verbose: bool){
	use std::fs::{create_dir, copy};

	for p in src1.clone().read_dir().expect("Failed to read the directory"){
		if let Ok(p) = p{
			let mut cur_path_suffix: PathBuf = Path::new(p.path().as_path().clone().strip_prefix(&src1.clone()).unwrap()).to_path_buf();

			let mut file_in_dest: PathBuf = Path::new(dest).join(cur_path_suffix.clone()).as_path().to_path_buf();
			let mut file_in_src2: PathBuf = Path::new(src2).join(cur_path_suffix.clone()).as_path().to_path_buf();

			if !p.path().as_path().is_dir() && !p.path().as_path().is_file(){//iterated path is nothing - would likely never happen
				continue;
			}

			if p.path().as_path().is_dir() && !file_in_dest.is_file() && !file_in_dest.is_dir(){
				create_dir(file_in_dest.clone()).expect(&format!("Failed creating directory {}", file_in_dest.clone().to_str().unwrap()));

				if verbose{ println!("dir {:?} -> create dir {:?}", p.path().as_path(), file_in_dest.clone()); }

				iter_dir_merge_copy(&p.path().as_path(), &file_in_src2.clone(), &file_in_dest.clone(), verbose);
			}



			if p.path().as_path().is_dir() && file_in_src2.is_dir(){
				if verbose{ println!("Enter: {:?}", p.path().as_path()); }

				iter_dir_merge_copy(&p.path().as_path(), &file_in_src2, &file_in_dest.clone(), verbose);
			}
			else if p.path().as_path().is_file() && file_in_src2.is_file(){//file to file - check which is newer
				if p.path().as_path().is_newer_than(&file_in_src2){
					copy(p.path().as_path(), file_in_dest.clone()).expect(&format!("Failed copying file {} to {}", p.path().as_path().to_str().unwrap(), file_in_dest.to_str().unwrap()));

					if verbose{ println!("file {:?} ->  file {:?}", p.path().as_path(), file_in_dest); }
				}
			}
			else if p.path().as_path().is_file() && file_in_src2.is_dir(){//from file to dir - check which is newer and if so ask to overwrite
				if p.path().as_path().is_newer_than(&file_in_src2){
					if yes_no_prompt(&format!("Overwrite directory \"{}\" with file \"{}\"", file_in_src2.to_str().unwrap(), p.path().as_path().to_str().unwrap())){
						copy(p.path().as_path(), file_in_dest.clone()).expect(&format!("Failed copying file "));
						if verbose{ println!("file {:?} ->  overwrite directory {:?}", p.path().as_path(), file_in_dest); }
					}
				}
			}
			else if p.path().as_path().is_dir() && file_in_src2.is_file(){//from dir to file - check which is newer and if so ask to overwrite
				if p.path().as_path().is_newer_than(&file_in_src2){
					if yes_no_prompt(&format!("Overwrite file \"{}\" with directory \"{}\"", file_in_dest.to_str().unwrap(), p.path().as_path().to_str().unwrap())){
						copy(p.path().as_path(), file_in_dest.clone()).unwrap();
						if verbose{ println!("directory {:?} ->  overwrite file {:?}", p.path().as_path(), file_in_dest); }
					}
				}
			}
			else{//file is not present in src2 copy it into dest
				if p.path().as_path().is_file(){
					copy(p.path().as_path(), file_in_dest.clone()).expect(&format!("Failed copying file {} to {}", p.path().as_path().to_str().unwrap(), file_in_dest.to_str().unwrap()));

					if verbose{ println!("file {:?} -> create file: {:?}", p.path().as_path(), file_in_dest); }
				}
			}
		}
	}
}

//TODO: Add gui written in Conrod

fn main(){
	use std::fs::create_dir;

    let mut src1: Option<PathBuf> = None;
	let mut src2: Option<PathBuf> = None;//by default merge will happen in src2
	let mut dest: Option<PathBuf> = None;

	//this will merge the items in the second directory and the remove the first directory
	//requires only two arguments
	let mut rm_srcdir: bool = false;

	//be verbose
	let mut verbose: bool = false;


	let mut i=0;
    for arg in std::env::args(){
        if i > 0{
            //read file
			if arg == "--verbose" || arg == "-v"{
				verbose = true;
			}
			else if arg == "--rm-srcdir"{
				rm_srcdir = true;
			}
			else if arg == "--merge-into"{
				if std::env::args().len() >= i+1{
					dest = Some(PathBuf::from(Path::new(&std::env::args().nth(i+1).unwrap())));
				}
				else{
					panic!("No merge directory given");
				}
			}
            else if src1.is_none(){//1. arg after flags
				src1 = Some(PathBuf::from(&arg));
            }
            //write file
            else if src2.is_none(){//2. arg after flags
                src2 = Some(PathBuf::from(&arg));
            }
        }
        i+=1;
    }

	if src1.is_none() || src2.is_none(){
		panic!("Insufficient number of args");
	}

	if dest.is_some() && !dest.clone().unwrap().is_dir(){
		create_dir(dest.clone().unwrap()).expect("Failed to created destination directory");
	}

	if !src1.clone().unwrap().is_dir() || !src2.clone().unwrap().is_dir() || dest.is_some() && !dest.clone().unwrap().is_dir(){
		panic!("No such directory");
	}


	if dest.is_some(){
		iter_dir_merge_copy(&src1.clone().unwrap(), &src2.clone().unwrap(), &dest.clone().unwrap(), verbose);
		iter_dir_merge_copy(&src2.clone().unwrap(), &src1.clone().unwrap(), &dest.clone().unwrap(), verbose);
	}
	else{
		iter_dir_merge_copy(&src1.clone().unwrap(), &src2.clone().unwrap(), &src2.clone().unwrap(), verbose);
		iter_dir_merge_copy(&src2.clone().unwrap(), &src1.clone().unwrap(), &src2.clone().unwrap(), verbose);
	}

	if rm_srcdir{
		std::fs::remove_dir_all(src1.clone().unwrap()).expect(&format!("Failed to remove directory {}", src1.clone().unwrap().to_str().unwrap()));
	}
}
