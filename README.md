Merges two folders(A and B) on each side with the latest files into folder C and creates folders which don't exist.
  
Compares each file in folder A with the files in folder B and if the file is more up to date in A then it will be copied into folder C
and then the same for the files in folder A, or just copied into B if set so.
  
Setup:  
Add ~/.cargo/ (or configured path) to your PATH variable when installing  
> git clone https://gitlab.com/nifker/merge_dirs_keep_latest  
> cd merge_dirs  
> cargo build --release  
> cargo run  

or  

> cargo install  
  
Usage:  
> merge_dirs <src1> <src2>  
> --rm-srcdir  removes the first source directory  
> --merge-into  merges the content into another directory  
> --verbose  -v  be verbose  
